#!/usr/bin/env python

# source : https://www.kaggle.com/willkoehrsen/start-here-a-gentle-introduction
import os
import numpy as np
import pandas as pd 
from sklearn.preprocessing import LabelEncoder

# Training data
app_train = pd.read_csv('../input/application_train.csv')
print('Training data shape: ', app_train.shape)

# Testing data features
app_test = pd.read_csv('../input/application_test.csv')
print('Testing data shape: ', app_test.shape)

# Function to calculate missing values by column# Funct 
# def missing_values_table(df):
#     # Total missing values
#     mis_val = df.isnull().sum()
    
#     # Percentage of missing values
#     mis_val_percent = 100 * df.isnull().sum() / len(df)
    
#     # Make a table with the results
#     mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
    
#     # Rename the columns
#     mis_val_table_ren_columns = mis_val_table.rename(
#     columns = {0 : 'Missing Values', 1 : '% of Total Values'})
    
#     # Sort the table by percentage of missing descending
#     mis_val_table_ren_columns = mis_val_table_ren_columns[
#         mis_val_table_ren_columns.iloc[:,1] != 0].sort_values(
#     '% of Total Values', ascending=False).round(1)
    
#     # Print some summary information
#     print ("Your selected dataframe has " + str(df.shape[1]) + " columns.\n"      
#         "There are " + str(mis_val_table_ren_columns.shape[0]) +
#           " columns that have missing values.")
    
#     # Return the dataframe with missing information
#     return mis_val_table_ren_columns

# missing_values = missing_values_table(app_train)

print ('*****ENCODING DATA*****')

# Create a label encoder object
le = LabelEncoder()
le_count = 0
col_headers = list(app_train)

# Iterate through the columns
for col in app_train:
    if app_train[col].dtype == 'object':
        # If 2 or fewer unique categories
        if len(list(app_train[col].unique())) <= 2:
            # Train on the training data
            le.fit(app_train[col])
            # Transform both training and testing data
            app_train[col] = le.transform(app_train[col])
            app_test[col] = le.transform(app_test[col])
            
            print ('Column %s used label encoding.' % col)
            # Keep track of how many columns were label encoded
            le_count += 1
        
# one-hot encoding of categorical variables
app_train = pd.get_dummies(app_train)
app_test = pd.get_dummies(app_test)

# after one-hot encoding
print('Training Features shape: ', app_train.shape)
print('Testing Features shape: ', app_test.shape)

train_labels = app_train['TARGET']

print ('*****ALIGN DATA*****')

# Align the training and testing data, keep only columns present in both dataframes
app_train, app_test = app_train.align(app_test, join = 'inner', axis = 1)

print('Training Features shape: ', app_train.shape)
print('Testing Features shape: ', app_test.shape)

# Add target back in to the data
app_train['TARGET'] = train_labels

# Find correlations with the target and sort
# correlations = app_train.corr()['TARGET'].sort_values()

# Display correlations
# print('Most Positive Correlations: \n', correlations.tail(15))
# print('\nMost Negative Correlations: \n', correlations.head(15))


from sklearn.preprocessing import MinMaxScaler, Imputer

# Drop the target from the training data
if 'TARGET' in app_train:
    train = app_train.drop(columns = ['TARGET'])
else:
    train = app_train.copy()
features = list(train.columns)

# Copy of the testing data
test = app_test.copy()

# Median imputation of missing values
imputer = Imputer(strategy = 'median')

# Scale each feature to 0-1
scaler = MinMaxScaler(feature_range = (0, 1))

print ('*****IMPUTATION*****')
# Fit on the training data
imputer.fit(train)

# Transform both training and testing data
train = imputer.transform(train)
test = imputer.transform(app_test)

print ('*****SCALING*****')
# Repeat with the scaler
scaler.fit(train)
train = scaler.transform(train)
test = scaler.transform(test)

print('Training data shape: ', train.shape)
print('Testing data shape: ', test.shape)

# from sklearn.linear_model import LogisticRegression
# log_reg = LogisticRegression(C = 0.0001)
# log_reg.fit(train, train_labels)
# log_reg_pred = log_reg.predict_proba(test)[:, 1]
# submit = app_test[['SK_ID_CURR']]
# submit['TARGET'] = log_reg_pred
# submit.to_csv('log_reg_baseline.csv', index = False)

# from sklearn.neural_network import MLPClassifier
# nn = MLPClassifier(solver='lbfgs', alpha=1e-5, 
#                    hidden_layer_sizes=(100), random_state=1)
# nn.fit(train, train_labels)
# nn_pred = nn.predict_proba(test)[:, 1]
# submit = app_test[['SK_ID_CURR']]
# submit['TARGET'] = nn_pred
# submit.to_csv('nn_baseline.csv', index = False)


# TORCH logistic regression
# import torch
# import torch.nn as nn
# import torchvision
# import torchvision.transforms as transforms

# # Hyper-parameters 
# input_size = 239
# num_classes = 2
# num_epochs = 700 #0.522
# batch_size = 100
# learning_rate = 0.001

# inputs = torch.from_numpy(train).float()
# labels = torch.from_numpy(train_labels.values)

# # Logistic regression model
# model = nn.Linear(input_size, num_classes)

# # Loss and optimizer
# # nn.CrossEntropyLoss() computes softmax internally
# criterion = nn.CrossEntropyLoss()
# optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)  

# # Train the model
# # total_step = len(train_loader)
# for epoch in range(num_epochs):
#     # Forward pass
#     outputs = model(inputs)
#     loss = criterion(outputs, labels)
    
#     # Backward and optimize
#     optimizer.zero_grad()
#     loss.backward()
#     optimizer.step()
    
#     # if (epoch+1) % batch_size == 0:
#     print ('Epoch [{}/{}], Loss: {:.4f}'.format(epoch+1, num_epochs, loss.item()))

# # Save the model checkpoint
# # torch.save(model.state_dict(), 'model.ckpt')

# test_inputs = torch.from_numpy(test).float()
# with torch.no_grad():
#     outputs = model(test_inputs)
#     # https://discuss.pytorch.org/t/how-to-extract-probabilities/2720/7
#     predicted = nn.functional.softmax(outputs)[:,1]

# submit = app_test[['SK_ID_CURR']]
# submit['TARGET'] = predicted
# submit.to_csv('log_reg_baseline_torch.csv', index = False)


# TORCH NEURAL NETWORK
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms

# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Hyper-parameters 
input_size = 239
hidden_size = 128
num_classes = 2
num_epochs = 230  # 0.696
batch_size = 100
learning_rate = 0.001

inputs = torch.from_numpy(train).float().to(device)
labels = torch.from_numpy(train_labels.values).to(device)

# Fully connected neural network with one hidden layer
class NeuralNet(nn.Module):
    def __init__(self, input_size, hidden_size, num_classes):
        super(NeuralNet, self).__init__()
        self.fc1 = nn.Linear(input_size, hidden_size) 
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(hidden_size, num_classes)
    
    def forward(self, x):
        out = self.fc1(x)
        out = self.relu(out)
        out = self.fc2(out)
        return out

model = NeuralNet(input_size, hidden_size, num_classes).to(device)

# Loss and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

# Train the model
# total_step = len(train_loader)
for epoch in range(num_epochs):
    # Forward pass
    outputs = model(inputs)
    loss = criterion(outputs, labels)
    
    # Backward and optimize
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    
    # if (epoch+1) % batch_size == 0:
    print ('Epoch [{}/{}], Loss: {:.4f}'.format(epoch+1, num_epochs, loss.item()))

# Save the model checkpoint
# torch.save(model.state_dict(), 'model.ckpt')

test_inputs = torch.from_numpy(test).float().to(device)
with torch.no_grad():
    outputs = model(test_inputs)
    # https://discuss.pytorch.org/t/how-to-extract-probabilities/2720/7
    predicted = nn.functional.softmax(outputs)[:,1]

submit = app_test[['SK_ID_CURR']]
submit['TARGET'] = predicted
submit.to_csv('nn_baseline_torch.csv', index = False)
